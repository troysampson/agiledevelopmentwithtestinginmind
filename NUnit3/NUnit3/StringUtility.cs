﻿using System.Text.RegularExpressions;

namespace NUnit3
{
    public class StringUtility
    {
        public string RemoveSpecialCharacters(string input)
        {
            return RegexStringUtility.RemoveSpecialCharacters(input);
        }

        public class RegexStringUtility
        {
            public static string RemoveSpecialCharacters(string input)
            {
                return Regex.Replace(input, "[^a-zA-Z0-9 -]", "");
            }
        }
    }
}
