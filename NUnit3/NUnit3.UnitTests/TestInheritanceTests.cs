﻿using NUnit.Framework;

namespace NUnit3.UnitTests
{
    [TestFixture]
    public class ParentTests
    {
        [Test]
        public void ParentTest() { }
    }

    [TestFixture]
    public class ChildTests : ParentTests
    {
        [Test]
        public void ChildTest() { }
    }
}
