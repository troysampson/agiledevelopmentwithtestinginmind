﻿using NUnit.Framework;

namespace NUnit3.UnitTests
{
    [TestFixture]
    public class StringUtilityTests
    {
        readonly StringUtility _stringUtility = new StringUtility();
        [Test]
        public void RemoveSpecialCharacters()
        {
            var value = "Cartoons swear with special characters %&@#";
            var expected = "Cartoons swear with special characters ";
            Assert.AreEqual(expected, _stringUtility.RemoveSpecialCharacters(value));
        }

        [TestFixture]
        public class RegexStringUtilityTests
        {
            [Test]
            public void RemoveSpecialCharacters()
            {
                var value = "Cartoons swear with special characters %&@#";
                var expected = "Cartoons swear with special characters ";
                Assert.AreEqual(expected, StringUtility.RegexStringUtility.RemoveSpecialCharacters(value));
            }
        }
    }
}
