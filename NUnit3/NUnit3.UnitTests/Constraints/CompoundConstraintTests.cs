﻿using NUnit.Framework;

namespace NUnit3.UnitTests.Constraints
{
    [TestFixture]
    public class CompoundConstraintTests
    {
        [Test]
        public void AndConstraintTest()
        {
            Assert.That("It is getting late", Does.StartWith("It").And.EndsWith("late"));
        }

        [Test]
        public void NotConstraintTest()
        {
            Assert.That(10, Is.Not.EqualTo(5));
        }

        [Test]
        public void OrConstraintTest()
        {
            Assert.That(3, Is.LessThan(5).Or.GreaterThan(10));
        }
    }
}
