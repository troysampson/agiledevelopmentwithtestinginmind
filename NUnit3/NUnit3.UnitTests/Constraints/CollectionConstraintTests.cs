﻿using System.Linq;
using NUnit.Framework;

namespace NUnit3.UnitTests.Constraints
{
    [TestFixture]
    public class CollectionConstraintTests
    {
        [Test]
        public void AllItemsTest()
        {
            var array = new[] { 1, 2, 3 };
            Assert.That(array, Is.All.LessThan(4));
            //compare to linq but the Test Results are not as good
            Assert.True(array.All(x => x < 4));
        }

        [Test]
        public void ContainsConstraintTest()
        {
            var array = new[] { "Tom", "Jerry" };
            Assert.That(array, Has.Member("Tom"));
            Assert.That(array, Contains.Item("Tom"));
            Assert.That(array, Does.Contain("Tom"));
            CollectionAssert.Contains(array, "Tom");
        }

        [Test]
        public void EquivalentConstraintTest()
        {
            var array1 = new[] { 'a', 'b', 'c' };
            var array2 = new[] { 'a', 'b', 'c' };
            var array3 = new[] { 'a', 'b' };

            Assert.That(array1, Is.EquivalentTo(array2));
            Assert.That(array1, Is.Not.EquivalentTo(array3));
            CollectionAssert.AreEquivalent(array1, array2);
            CollectionAssert.AreNotEquivalent(array1, array3);
        }

        [Test]
        public void OrderedConstraintTest()
        {
            var array1 = new[] { 1, 2, 3, 4 };
            var array2 = new[] { 5, 2, 6, 3 };

            Assert.That(array1, Is.Ordered);
            Assert.That(array2, Is.Not.Ordered);
            CollectionAssert.IsOrdered(array1);
            // CollectionAssert does not have an IsNotOrdered
            // Uses default IComparer for the implementation of being ordered
        }

        [Test]
        public void SubSetConstraintTest()
        {
            var array1 = new[] { 1, 2 };
            var array2 = new[] { 1, 2, 3, 4, 5 };
            var array3 = new[] { 3, 4, 5 };

            Assert.That(array1, Is.SubsetOf(array2));
            Assert.That(array1, Is.Not.SubsetOf(array3));
            CollectionAssert.IsSubsetOf(array1, array2);
            CollectionAssert.IsNotSubsetOf(array1, array3);
        }

        [Test]
        public void SuperSetConstraintTest()
        {
            var array1 = new[] { 1, 2 };
            var array2 = new[] { 1, 2, 3, 4, 5 };
            var array3 = new[] { 3, 4, 5 };

            Assert.That(array2, Is.SupersetOf(array1).And.SupersetOf(array3));
            Assert.That(array1, Is.Not.SupersetOf(array2));
            CollectionAssert.IsSupersetOf(array2, array1);
            CollectionAssert.IsNotSupersetOf(array1, array2);
        }

        [Test]
        public void EmptyConstraintTest()
        {
            var array1 = new object[] { };
            var array2 = new[] { 1 };

            Assert.That(array1, Is.Empty);
            Assert.That(array2, Is.Not.Empty);
            CollectionAssert.IsEmpty(array1);
            CollectionAssert.IsNotEmpty(array2);
        }

        [Test]
        public void ExactCountConstraintTest()
        {
            var array1 = new[] { 1, 2, 3 };

            Assert.That(array1, Has.Exactly(3).LessThan(4));
        }

        [Test]
        public void NoItemConstraintTest()
        {
            var integerArray = new[] { 1, 2, 3 };

            Assert.That(integerArray, Has.None.EqualTo(4));
            Assert.That(integerArray, Has.No.EqualTo(4));
        }

        [Test]
        public void SomeItemConstraintTest()
        {
            var charArray = new[] { 'a', 'b', 'c' };

            Assert.That(charArray, Has.Some.EqualTo('a'));
        }

        [Test]
        public void UniqueconstraintTest()
        {
            var array = new[] { 1, 2, 3, 4, 5 };
            var array2 = new[] { 1, 2, 3, 4, 5, 1 };

            Assert.That(array, Is.Unique);
            Assert.That(array2, Is.Not.Unique);
        }
    }
}
