﻿using NUnit.Framework;

namespace NUnit3.UnitTests.Constraints
{
    [TestFixture]
    public class StringConstraintTests
    {
        [Test]
        public void EmptyStringConstraintTest()
        {
            Assert.That(string.Empty, Is.Empty);
            Assert.That("A String", Is.Not.Empty);
        }

        [Test]
        public void EndsWithConstraintTest()
        {
            Assert.That("Run tests before checking in code", Does.EndWith("code"));
        }

        [Test]
        public void RegexConstraintTest()
        {
            const string phonePattern = @"\d{3}\-?\d{3}\-?\d{4}";
            Assert.That("801-555-5555", Does.Match(phonePattern));
            Assert.That("8015555555", Does.Match(phonePattern));
        }

        [Test]
        public void StartsWithConstraintTest()
        {
            Assert.That("Between a rock and a hard place", Does.StartWith("B"));
        }

        [Test]
        public void SubstringConstraintTest()
        {
            Assert.That("Between a rock and a hard place", Does.Contain("rock"));
        }
    }
}
