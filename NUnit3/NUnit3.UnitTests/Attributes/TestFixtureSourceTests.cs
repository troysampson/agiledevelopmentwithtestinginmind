﻿using System.Collections;
using NUnit.Framework;

namespace NUnit3.UnitTests.Attributes
{
    //Used to grab dependencies for class (there are better ways to do this)
    [TestFixtureSource("FixtureArgs")]
    public class TestFixtureSource1Tests
    {
        #region Dependencies
        int _value1 { get; set; }
        char _value2 { get; set; }
        #endregion

        public TestFixtureSource1Tests(int value1, char value2)
        {
            _value1 = value1;
            _value2 = value2;
        }

        [Test]
        public void DependenciesTest1()
        {
            CollectionAssert.Contains(FixtureArgs, new object[] { _value1, _value2 });
        }

        /// <summary>
        /// It may be a field, property or method in the test class.
        /// It must be static.
        /// It must return an IEnumerable or a type that implements IEnumerable.For fields an array is generally used. For properties and methods, you may return an array or implement your own iterator.
        /// The individual items returned by the enumerator must either be object arrays or implement ITestFixtureData.Arguments must be consistent with the fixture constructor.
        /// </summary>
        public static object[] FixtureArgs = { new object[] { 1, 'A' }, new object[] { 2, 'B' } };
    }

    [TestFixtureSource(typeof(FixtureArgs2), "FixtureObjects")]
    public class TestFixtureSource2Tests
    {
        #region Dependencies
        int _value1 { get; set; }
        char _value2 { get; set; }
        #endregion

        public TestFixtureSource2Tests(int value1, char value2)
        {
            _value1 = value1;
            _value2 = value2;
        }

        [Test]
        public void DependenciesTest2()
        {
            CollectionAssert.Contains(FixtureArgs2.FixtureObjects, new object[] { _value1, _value2 });
        }
    }

    [TestFixtureSource(typeof(FixtureArgs3))]
    public class TestFixtureSource3Tests
    {
        #region Dependencies
        int _value1 { get; set; }
        char _value2 { get; set; }
        #endregion

        public TestFixtureSource3Tests(int value1, char value2)
        {
            _value1 = value1;
            _value2 = value2;
        }

        [Test]
        public void DependenciesTest3()
        {
            CollectionAssert.Contains(TestFixtureSource1Tests.FixtureArgs, new object[] { _value1, _value2 });
        }
    }

    public class FixtureArgs2
    {
        public static object[] FixtureObjects =
        {
            new object[] {1, 'A'},
            new object[] { 2, 'B' }
        };
    }
    /// <summary>
    /// It may be a field, property or method in the test class.
    /// It must be static.
    /// It must return an IEnumerable or a type that implements IEnumerable.For fields an array is generally used. For properties and methods, you may return an array or implement your own iterator.
    /// The individual items returned by the enumerator must either be object arrays or implement ITestFixtureData.Arguments must be consistent with the fixture constructor.
    /// </summary> 
    public class FixtureArgs3 : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return new object[] { 1, 'A' };
            yield return new object[] { 2, 'B' };
        }
    }
}
