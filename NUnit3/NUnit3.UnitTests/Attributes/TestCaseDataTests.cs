using System.Collections;
using NUnit.Framework;

namespace NUnit3.UnitTests.Attributes
{
    [TestFixture]
    public class TestCaseDataTests
    {
        [Test, TestCaseSource(typeof(MyDataClass), nameof(MyDataClass.TestCases))]
        public int DivideTest(int numerator, int denominator)
        {
            return numerator / denominator;
        }
    }

    public class MyDataClass
    {
        public static IEnumerable TestCases
        {
            get
            {
                yield return new TestCaseData(12, 3).Returns(4).SetName("TestCase 1");
                yield return new TestCaseData(12, 2).Returns(6).SetName("TestCase 2");
                yield return new TestCaseData(12, 4).Returns(3).SetName("TestCase 3");
            }
        }
    }
}