using System;
using NUnit.Framework;

namespace NUnit3.UnitTests.Attributes
{
    [TestFixture]
    public class SetUpTearDownTests
    {
        static DisposableResource _staticResource;
        DisposableResource _classResource;
        
        [OneTimeSetUp] //Previously known as TestFixtureSetUp in version 2.6.4
        public void OneTimeSetup()
        {
            _staticResource = new DisposableResource();
            _staticResource.String += "StaticResource";
        }

        [OneTimeTearDown] //Previously known as TestFixtureTearDown in version 2.6.4
        public void OneTimeTearDown()
        {
            _staticResource.Dispose();
        }

        [SetUp]
        public void Setup()
        {
            _classResource = new DisposableResource();
            _classResource.String += "ClassResource";
        }

        [TearDown]
        public void TearDown()
        {
            _classResource.Dispose();
        }

        [Test]
        public void Test()
        {
            Assert.AreEqual("StaticResourceClassResource", _staticResource.String + _classResource.String);
        }
    }

    internal class DisposableResource : IDisposable
    {
        public string String { get; set; }
        public void Dispose() { }
    }
}