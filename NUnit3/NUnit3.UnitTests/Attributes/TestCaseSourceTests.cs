﻿using System.Collections;
using NUnit.Framework;

namespace NUnit3.UnitTests.Attributes
{
    [TestFixture]
    public class TestCaseSourceTests
    {
        [TestCaseSource(nameof(DivideCases))]
        public void DivideTest(int numerator, int denominator, int expected)
        {
            Assert.AreEqual(expected, numerator / denominator);
        }

        [TestCaseSource(typeof(AnotherClass), nameof(AnotherClass.DivideCases))]
        public void DivideTest2(int numerator, int divisor, int expected)
        {
            Assert.That(expected, Is.EqualTo(numerator / divisor));
        }

        [TestCaseSource(typeof(DivideCases2))]
        public void DivideTest3(int numerator, int divisor, int expected)
        {
            Assert.That(expected, Is.EqualTo(numerator / divisor));
        }

        static readonly object[] DivideCases =
            {
                new object[] { 12, 3, 4 },
                new object[] { 12, 2, 6 },
                new object[] { 12, 4, 3 }
            };
    }

    public class AnotherClass
    {
        public static object[] DivideCases =
        {
            new object[] { 12, 3, 4 },
            new object[] { 12, 2, 6 },
            new object[] { 12, 4, 3 }
        };
    }

    public class DivideCases2 : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return new object[] { 12, 3, 4 };
            yield return new object[] { 12, 2, 6 };
            yield return new object[] { 12, 4, 3 };
        }
    }
}
