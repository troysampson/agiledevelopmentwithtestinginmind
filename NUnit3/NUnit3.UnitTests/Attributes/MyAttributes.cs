﻿using System.Linq;
using NUnit.Framework;

namespace NUnit3.UnitTests.Attributes
{
    [TestFixture(Author = "Troy Sampson", Description = "Test class used for showing useful attibutes", TestOf = typeof(ClassUnderTest))]
    //[Author("Troy Sampson")]
    //[Description("Test class used for showing useful attibutes")]
    //[TestOf(typeof(ClassUnderTest))]
    //Do not name a test class just "Tests" - Resharper will barf
    public class MyAttributes
    {
        [MaxTime(2000)] //(used for long running tests - why do they run long - should this really be used?)
        [Repeat(5)] //(used to exercise a tests x times and report if any fail - why are they finicky - should this really be used?)
        [Test(ExpectedResult = true)]
        public bool SpeedTest()
        { return true; }

        [Test]
        [Retry(5)] //(used to tests a finicky test up to x times and report if all fail - why are they finicky - should this really be used?)
        [Ignore("Forgot what I was going to test", Until = "2016-3-5")]
        public void TroysTest() { }

        //TestName only available on TestCase attributes
        [TestCase(1, 2, 3, ExpectedResult = 6, TestName = "Adding three numbers")]
        [TestCase(1, 2, 3, 4, ExpectedResult = 10, TestName = "Adding four numbers")]
        public int Add(params int[] values)
        {
            return values.Sum();
        }
    }
}