﻿using NUnit.Framework;

namespace NUnit3.UnitTests.Attributes
{
    [TestFixture(Author = "Troy Sampson")]
    public class CollectionAttributeTests
    {
        /// <summary>
        /// Test all possible combinations
        /// </summary>
        [Test, Combinatorial]
        public void CombinatorialTests([Values(1, 2, 3)] int value1, [Values('A', 'B')] char value) { }

        /// <summary>
        /// Used to test that all possible pairs are used (without respect to the number of parameters) heuristic algorithm to reduce the number of tests
        /// </summary>
        [Test, Pairwise]
        public void PairwiseTests([Values('A', 'B', 'C')] char value1, [Values('+', '-')] char value2, [Values('X', 'Y')] char value3) { }

        /// <summary>
        /// Used to test Random values within a range
        /// </summary>
        [Test]
        public void RandomTests([Random(0, 10, 5)] int value1) { }

        // Used to test a Range of values
        [Test]
        public void RangeTests([Range(0, 10, 2)] int value1) { }

        // Used to test an ordered set of tests
        [Test, Sequential]
        public void SequentialTests([Values(1, 2, 3)] int value1, [Values('A', 'B', 'C')] char value2) { }

        // Values Attribute used to contain test case parameters.  Default is a combination when used with multiple value attributes
        [Test]
        public void ValueTests([Values(1, 2, 3)] int value1, [Values('A', 'B')] char value2) { }
    }
}
