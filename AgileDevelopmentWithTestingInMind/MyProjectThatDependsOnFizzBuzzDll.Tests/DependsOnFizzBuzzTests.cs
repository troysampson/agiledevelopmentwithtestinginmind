﻿using System.Reflection;
using Moq;
using NUnit.Framework;

namespace MyProjectThatDependsOnFizzBuzzDll.Tests
{
    [TestFixture]
    public class DependsOnFizzBuzzTests
    {
        private DependsOnFizzBuzz _dependsOnFizzBuzz;
        private Mock<IFizzBuzz> _fizzBuzzMock;

        [SetUp]
        public void Setup()
        {
            _fizzBuzzMock = new Mock<IFizzBuzz>();
            _dependsOnFizzBuzz = new DependsOnFizzBuzz(_fizzBuzzMock.Object);
        }

        [Test]
        public void CallFizzBuzzTest()
        {
            const int value = 5;
            const string expectedString = "Buzz";
            _fizzBuzzMock.Setup(x => x.CreateString(value)).Returns(expectedString).Verifiable();
            var actual = _dependsOnFizzBuzz.CallFizzBuzz(value);
            Assert.AreEqual(expectedString, actual);
        }
    }
}
