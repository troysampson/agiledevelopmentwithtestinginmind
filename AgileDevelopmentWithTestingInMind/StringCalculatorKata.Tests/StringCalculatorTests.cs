﻿using System;
using NUnit.Framework;

namespace StringCalculatorKata.Tests
{
    [TestFixture]
    public class StringCalculatorTests
    {
        private StringCalculator _stringCalculator;

        [SetUp]
        public void Setup()
        {
            _stringCalculator = new StringCalculator();
        }

        [TearDown]
        public void TearDown()
        {
            _stringCalculator = null;
        }

        [TestCase("", Result = 0, TestName = "Empty returns 0")]
        [TestCase("1", Result = 1, TestName = "One Number: 1 returns 1")]
        [TestCase("1,2", Result = 3, TestName = "Two Numbers: 1,2 returns 1")]
        [TestCase("1\n2,3", Result = 6, TestName = "Allow Newlines: 1\n2,3 returns 6")]
        [TestCase("//;\n1;2", Result = 3, TestName = "Allow Customized Delimeters: //:\n1;2 returns 3")]
        [TestCase("//;\n1;-2", ExpectedException = typeof(Exception), MatchType = MessageMatch.Contains, ExpectedMessage = "negatives not allowed",TestName = "Negative Number Throws Exception: //:\n1;2 throws exception")]
        [TestCase("//;\n1001;2", Result = 2, TestName = "Numbers Greater than 1000 are Omitted: //:\n1001;2 returns 2")]
        [TestCase("//[***]\n1***2***3", Result = 6, TestName = "Delimeters of Any Length: //[***]\n1***2***3 returns 6")]
        [TestCase("//[*][%]\n1*2%3", Result = 6, TestName = "Multiple Delimeters of Any Length: //[*][%]\n1*2%3 returns 6")]
        public int AddTest(string value)
        {
            return _stringCalculator.Add(value);
        }
    }
}
