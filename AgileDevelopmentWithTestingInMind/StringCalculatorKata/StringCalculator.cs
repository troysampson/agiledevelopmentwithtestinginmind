﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace StringCalculatorKata
{
    public class StringCalculator
    {
        public int Add(string value)
        {
            string[] delimeters;
            string numbers;
            GetDelimetersAndNumberStrings(value, out delimeters, out numbers);
            if (delimeters == null)
            {
                delimeters = new[] { ",", "\n" };
            }
            var numberStrings = numbers.Split(delimeters, StringSplitOptions.RemoveEmptyEntries);
            var numberList = numberStrings.Select(x => Convert.ToInt32(x)).ToList();
            if (numberList.Any(x => x < 0)) { throw new Exception("negatives not allowed"); }
            return numberList.Where(x => x <= 1000).Sum();
        }

        private void GetDelimetersAndNumberStrings(string value, out string[] delimeters, out string numbers)
        {
            var match = Regex.Match(value, @"^//(?<delimeters>.+?)\n(?<numbers>.*)$");
            if (!match.Success)
            {
                delimeters = null;
                numbers = value;
            }
            else
            {
                delimeters = match.Groups["delimeters"].Value.ToCharArray().Select(x => x.ToString()).ToArray();
                numbers = match.Groups["numbers"].Value;
            }
        }
    }
}
