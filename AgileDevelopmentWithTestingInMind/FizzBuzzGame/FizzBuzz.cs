﻿using System;
using System.Globalization;

namespace FizzBuzzGame
{
    public class FizzBuzz
    {
        public static string CreateString(int value)
        {
            if (value < 1) { throw new ArgumentOutOfRangeException("Argument cannot be less than 1"); }
            if (value % 20 == 0) { return "FizzBuzz"; }
            if (value % 4 == 0) { return "Fizz"; }
            if (value % 5 == 0) { return "Buzz"; }
            return value.ToString(CultureInfo.InvariantCulture);
        }
    }
}
