﻿namespace MyProjectThatDependsOnFizzBuzzDll
{
    public class DependsOnFizzBuzz
    {
        private IFizzBuzz _fizzBuzz;

        public DependsOnFizzBuzz(IFizzBuzz fizzBuzz)
        {
            _fizzBuzz = fizzBuzz;
        }

        public string CallFizzBuzz(int value)
        {
            return _fizzBuzz.CreateString(value);
        }
    }
}
