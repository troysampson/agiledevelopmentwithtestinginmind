﻿namespace MyProjectThatDependsOnFizzBuzzDll
{
    public interface IFizzBuzz
    {
        string CreateString(int value);
    }
}
