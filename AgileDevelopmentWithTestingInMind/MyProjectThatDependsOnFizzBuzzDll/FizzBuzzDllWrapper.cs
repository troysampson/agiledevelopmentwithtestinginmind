﻿using FizzBuzzGame;

namespace MyProjectThatDependsOnFizzBuzzDll
{
    public class FizzBuzzDllWrapper : IFizzBuzz
    {
        public string CreateString(int value)
        {
            return FizzBuzz.CreateString(value);
        }
    }
}