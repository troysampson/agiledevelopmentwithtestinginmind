﻿using System;
using NUnit.Framework;

namespace FizzBuzzGame.Tests
{
    [TestFixture]
    public class FizzBuzzTests
    {
        [TestCase(1, Result = "1", TestName = "1 Should Return 1")]
        [TestCase(3, Result = "Fizz", TestName = "3 Should Return Fizz")]
        [TestCase(5, Result = "Buzz", TestName = "5 Should Return Buzz")]
        [TestCase(15, Result = "FizzBuzz", TestName = "15 Should Return FizzBuzz")]
        [TestCase(0, ExpectedException = typeof(ArgumentOutOfRangeException), MatchType = MessageMatch.Contains, ExpectedMessage = "Argument cannot be less than 1", TestName = "0 Should throw ArgumentOutOfRangeException")]
        public string CreateStringTest(int value)
        {
            return FizzBuzz.CreateString(value);
        }
    }
}
