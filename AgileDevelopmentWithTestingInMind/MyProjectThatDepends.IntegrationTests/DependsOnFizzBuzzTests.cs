﻿using System;
using FizzBuzzGame;
using MyProjectThatDependsOnFizzBuzzDll;
using NUnit.Framework;

namespace MyProjectThatDepends.IntegrationTests
{
    [TestFixture]
    public class DependsOnFizzBuzzIntegrationTests
    {
        private DependsOnFizzBuzz _dependsOnFizzBuzz;

        [SetUp]
        public void Setup()
        {
            _dependsOnFizzBuzz = new DependsOnFizzBuzz(new FizzBuzzDllWrapper());
        }

        [TestCase(1, Result = "1", TestName = "1 Should Return 1")]
        [TestCase(4, Result = "Fizz", TestName = "3 Should Return Fizz")]
        [TestCase(5, Result = "Buzz", TestName = "5 Should Return Buzz")]
        [TestCase(20, Result = "FizzBuzz", TestName = "15 Should Return FizzBuzz")]
        [TestCase(0, ExpectedException = typeof(ArgumentOutOfRangeException), MatchType = MessageMatch.Contains, ExpectedMessage = "Argument cannot be less than 1", TestName = "0 Should throw ArgumentOutOfRangeException")]
        public string CallFizzBuzzTest(int value)
        {
            return _dependsOnFizzBuzz.CallFizzBuzz(value);
        }
    }
}
